package business;

import org.junit.Test;

import exception.NegativeNumberInputException;
import junit.framework.Assert;

public class CalculatorTest {

	@Test
	public void should_return_zero_when_empty_string() throws NegativeNumberInputException {
		Assert.assertEquals(0, Calculator.add(""));
	}

	@Test
	public void should_return_zero_when_string_is_null() throws NegativeNumberInputException {
		Assert.assertEquals(0, Calculator.add(null));
	}

	@Test
	public void should_return_simple_value_when_simple_number_string() throws NegativeNumberInputException {
		Assert.assertEquals(5, Calculator.add("5"));
	}

	@Test
	public void should_return_sum_value_when_double_number_string_comma_separated() throws NegativeNumberInputException {
		Assert.assertEquals(7, Calculator.add("2,5"));
	}

	@Test
	public void should_return_sum_value_when_double_number_string_new_line_separated() throws NegativeNumberInputException {
		Assert.assertEquals(7, Calculator.add("2\n5"));
	}

	@Test
	public void should_return_sum_value_when_more_than_two_numbers_string_same_delimiter() throws NegativeNumberInputException {
		Assert.assertEquals(8, Calculator.add("2,5,1"));
	}

	@Test
	public void should_return_sum_value_when_more_than_two_numbers_string_hybrid_delimiter() throws NegativeNumberInputException {
		Assert.assertEquals(8, Calculator.add("2\n5,1"));
	}

	@Test(expected = NegativeNumberInputException.class)
	public void should_throw_negative_number_exception() throws NegativeNumberInputException {
		Calculator.add("-2,5");
	}

	@Test
	public void should_ignore_numbers_greater_than_1000() throws NegativeNumberInputException {
		Assert.assertEquals(7, Calculator.add("2\n5,1001"));
	}

	@Test(expected = NumberFormatException.class)
	public void should_throw_number_format_format_exception() throws NegativeNumberInputException {
		Calculator.add("2,not_a_number");
	}
}
