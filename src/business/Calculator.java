package business;

import exception.NegativeNumberInputException;

public class Calculator {

	public static int add(String numbersToAdd) throws NegativeNumberInputException {
		if (!isValidString(numbersToAdd)) {
			return 0;
		}
		
		return addNumbers(numbersToAdd.split(",|\n"));
	}

	private static boolean isValidString(String numbersToProcess) {
		if (numbersToProcess == null || numbersToProcess.isEmpty()) {
			return false;
		}
		
		return true;
	}

	private static int addNumbers(String[] numbers) throws NegativeNumberInputException {
		int sum = 0;

		for (String number : numbers) {
			if (isValidNumber(number)) {
				sum += Integer.valueOf(number);
			}
		}

		return sum;
	}

	private static boolean isValidNumber(String number) throws NegativeNumberInputException {
		int i = Integer.valueOf(number);

		if (i < 0) {
			throw new NegativeNumberInputException();
		}

		if (i > 1000) {
			return false;
		}

		return true;
	}
}
